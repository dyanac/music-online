package is1.repository;

import is1.model.MyUser;

public interface MyUserRepository extends BaseRepository<MyUser, Long> {
	public MyUser findByUsername(String username);
}
