package is1.repository.jpa;

import javax.inject.Inject;
import javax.persistence.PersistenceException;

import is1.model.MyUser;
import is1.repository.MyUserRepository;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class jpaMyUserRepository extends jpaBaseRepository<MyUser, Long> implements MyUserRepository{
	
	@Inject
	private PasswordEncoder passwordEncoder;

	@Override
	@Transactional
	public MyUser save(MyUser user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		entityManager.persist(user);
		return user;
	}

	public MyUser findByUsername(String username) {
		try {
			return entityManager
					.createNamedQuery(MyUser.FIND_BY_USERNAME, MyUser.class)
					.setParameter("username", username).getSingleResult();
		} catch (PersistenceException e) {
			return null;
		}
	}
	
}
