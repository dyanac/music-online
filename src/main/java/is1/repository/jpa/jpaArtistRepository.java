package is1.repository.jpa;

import is1.model.Artist;
import is1.repository.ArtistRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class jpaArtistRepository extends jpaBaseRepository<Artist, Long> implements ArtistRepository{

	
	@Override
	public boolean insertGroup()
	{
		return Boolean.TRUE;
	};
	
	@Override
	public boolean deleteGroup(){
		return Boolean.TRUE;
	}; 
	
	
	@Override	
	public Artist save(Artist e) {		
		return e;
	}
	
	@Override
	public Boolean remove(Artist entity) {
		// borrar album
		return Boolean.TRUE;
	}	
	
	@Override
	public Boolean removeById(Long id) {
		Artist e = findById(id);
		return remove(e);
	}
	
}
