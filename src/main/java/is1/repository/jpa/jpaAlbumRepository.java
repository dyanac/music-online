package is1.repository.jpa;

import java.util.List;

import javax.persistence.PersistenceException;

import is1.model.Album;
import is1.repository.AlbumRepository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class jpaAlbumRepository extends jpaBaseRepository<Album, Long> implements AlbumRepository{
	
	
	@Override
	public List<Album> topTenLastWeek() {
		String jpql = "SELECT a FROM album a ORDER BY a.rate DESC LIMIT 0,10";
		try {
            return entityManager
                    .createQuery(jpql, Album.class)
                    .setFirstResult(0)
                    .setMaxResults(10)
                    .getResultList();
        } catch (PersistenceException e) {return null;}
		
	}
	/*
	@Override	
	public Album save(Album e) {		
		return e;
	}
	
	@Override
	public Boolean remove(Album entity) {
		return Boolean.TRUE;
	}	
	
	@Override
	public Boolean removeById(Long id) {
		Album e = findById(id);
		return remove(e);
	}
	*/
}
