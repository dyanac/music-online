package is1.repository.jpa;

import is1.model.Song;
import is1.repository.SongRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceException;
import java.util.List;

@Repository
@Transactional(readOnly = true)
public class jpaSongRepository extends jpaBaseRepository<Song, Long> implements SongRepository{
	@Override
	public Long timesPlayed() {
		return 0L;
	}
	
	public List<Song> getSearch(String titulo) {
        
        String jpql = "SELECT s FROM song, user u s WHERE s.name LIKE '%"+titulo+"%'";
        try {
            return entityManager
                    .createQuery(jpql, Song.class)
                    .setFirstResult(0)
                    .setMaxResults(50)
                    .getResultList();
        } catch (PersistenceException e) {return null;}
    }
	
	public List<Song> getBestRanked() {
		String jpql = "SELECT s FROM song s ORDER BY s.rate DESC LIMIT 0,20";
		try {
            return entityManager
                    .createQuery(jpql, Song.class)
                    .setFirstResult(0)
                    .setMaxResults(20)
                    .getResultList();
        } catch (PersistenceException e) {return null;}
	}
	
	/*
	@Override	
	public Song save(Song e) {		
		return e;
	}
	
	@Override
	public Boolean remove(Song entity) {
		return Boolean.TRUE;
	}	
	
	@Override
	public Boolean removeById(Long id) {
		Song e = findById(id);
		return remove(e);
	}
	
	
	@Override
	public long count() {
		return 0L;
	}
	*/
}
