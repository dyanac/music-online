package is1.repository.jpa;

import java.util.List;

import javax.persistence.PersistenceException;

import is1.model.Playlist;
import is1.model.Song;
//import is1.model.MyUser;
import is1.repository.PlaylistRepository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class jpaPlaylistRepository extends jpaBaseRepository<Playlist,Long> implements PlaylistRepository{
	
	@Override
	public void play(){
		
	};
	
	@Override
	public void next(){
		
	};
	
	@Override
	public void prev(){
			
	};
	
	@Override
	public List<Playlist> getBestRanked(){
		String jpql = "SELECT p FROM playlist p ORDER BY p.rate DESC LIMIT 0,20";
			try {
	            return entityManager
	                    .createQuery(jpql, Playlist.class)
	                    .setFirstResult(0)
	                    .setMaxResults(20)
	                    .getResultList();
	        } catch (PersistenceException e) {return null;}
	}
	
	@Override
	public List<Playlist> getSearch(String nombre, String usuarioId){
		String jpql = "SELECT p FROM playlist WHERE p.name LIKE '%"+nombre+"%' and user_id="+usuarioId;
		try {
            return entityManager
                    .createQuery(jpql, Playlist.class)
                    .setFirstResult(0)
                    .setMaxResults(20)
                    .getResultList();
        } catch (PersistenceException e) {return null;}
	}
	
	/*
	@Override	
	public Playlist save(Playlist e) {		
		return e;
	}
	
	@Override
	public Boolean remove(Playlist entity) {
		// borrar album
		return Boolean.TRUE;
	}	
	
	@Override
	public Boolean removeById(Long id) {
		Playlist e = findById(id);
		return remove(e);
	}
*/
}
