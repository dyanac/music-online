package is1.repository;

import is1.model.Playlist;
//import is1.model.MyUser;

import java.util.List;

public interface PlaylistRepository extends BaseRepository<Playlist, Long>{
	void play();
	void next();
	void prev();
	List<Playlist> getBestRanked();
	List<Playlist> getSearch(String nombre, String usuarioId);
}
