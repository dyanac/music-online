package is1.repository;

import java.util.List;

public interface BaseRepository<E,K> { //<entidad,pk>
	E save(E e);
	Boolean remove(E e);
	Boolean removeById(K id);
	E findById(K id);
	List<E> findEntries(int from, int total);
	E merge(E entity);
	long count();
}
