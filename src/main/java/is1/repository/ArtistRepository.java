package is1.repository;

import is1.model.Artist;

public interface ArtistRepository extends BaseRepository<Artist, Long> {
	boolean insertGroup(); //recive un grupo y la fecha en la cual toco en ese
	boolean deleteGroup(); //elimina el registro del grupo
	//list<Group> getGroups(); 
}
