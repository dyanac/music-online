package is1.repository;

import is1.model.Song;

import java.util.List;

public interface SongRepository extends BaseRepository<Song, Long> {
	Long timesPlayed();
	List<Song> getSearch(String query);
	List<Song> getBestRanked();
}
