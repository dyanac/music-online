package is1.service;

import is1.model.Playlist;
import is1.repository.PlaylistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlaylistService{

    @Autowired
    private PlaylistRepository playlistRepository;

    public Playlist createPlayList(Playlist playList) {
        playlistRepository.save(playList);
        return playList;
    }

    public Playlist getPlayList(Long id) {
        return playlistRepository.findById(id);
    }

    public List<Playlist> getPlayLists(int firstResult, int maxResults) {
        return playlistRepository.findEntries(firstResult, maxResults);
    }

}
