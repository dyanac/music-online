package is1.service;

import java.util.List;
import is1.model.Album;
import is1.repository.AlbumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AlbumService {
	
	@Autowired
    private AlbumRepository albumRepository;

    public Album createAlbum(Album album) {
        albumRepository.save(album);
        return album;
    }
    
    public Album getAlbum(Long id) {
        return albumRepository.findById(id);
    }

    public List<Album> getAlbums(int firstResult, int maxResults) {
        return albumRepository.findEntries(firstResult, maxResults);
    }
}
