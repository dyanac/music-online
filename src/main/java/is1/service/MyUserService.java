package is1.service;

import is1.model.MyUser;
import is1.repository.MyUserRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class MyUserService{

	@Autowired
    private MyUserRepository userRepository;

    public MyUser createUser(String username, String email, String password) {
    	MyUser account = new MyUser(username, email, password);
        userRepository.save(account);
        return account;
    }

    public MyUser createUser(MyUser user) {
        userRepository.save(user);
        return user;
    }

    public MyUser getAccount(long id) {
        return userRepository.findById(id);
    }

    public MyUser getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

}
