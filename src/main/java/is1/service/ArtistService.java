package is1.service;

import java.util.List;
import is1.model.Artist;
import is1.repository.ArtistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ArtistService {
	@Autowired
    private ArtistRepository artistRepository;
	
	public Artist createAuthor(Artist artist) {
		artistRepository.save(artist);
        return artist;
    }

    public Artist getArtist(Long id) {
        return artistRepository.findById(id);
    }

    public List<Artist> getAuthors(int firstResult, int maxResults) {
        return artistRepository.findEntries(firstResult, maxResults);
    }
}
