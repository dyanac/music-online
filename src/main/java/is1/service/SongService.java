package is1.service;

import java.util.List;

import is1.model.Song;
import is1.repository.SongRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SongService{

    @Autowired
    private SongRepository songRepository;

    public Song createSong(Song song) {
        songRepository.save(song);
        return song;
    }
    
    public Song getSong(Long id) {
        return songRepository.findById(id);
    }
    
    public List<Song> getSongs(int firstResult, int maxResults) {
        return songRepository.findEntries(firstResult, maxResults);
    }
}