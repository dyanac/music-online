package is1.bean;

import is1.model.Album;
import org.hibernate.validator.constraints.NotBlank;

public class AlbumBean {
	
	@NotBlank
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Album createAlbum() {
        return new Album(name);
    }
}
