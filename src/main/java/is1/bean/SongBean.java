package is1.bean;

import is1.model.Song;
import org.hibernate.validator.constraints.NotBlank;

public class SongBean {
	
	@NotBlank
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Song createSong() {
        return new Song(name);
    }
}
