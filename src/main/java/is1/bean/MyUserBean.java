package is1.bean;

import is1.model.MyUser;
import org.hibernate.validator.constraints.NotBlank;

public class MyUserBean {
	
	@NotBlank
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MyUser createSong() {
        return new MyUser(name);
    }
}
