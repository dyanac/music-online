package is1.bean;

import is1.model.Playlist;
import org.hibernate.validator.constraints.NotBlank;

public class PlaylistBean {
	
	@NotBlank
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Playlist createSong() {
        return new Playlist(name);
    }
}
