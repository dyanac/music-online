package is1.bean;

import is1.model.Artist;
import org.hibernate.validator.constraints.NotBlank;

public class ArtistBean {
	
	@NotBlank
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Artist createArtist() {
        return new Artist(name);
    }
}
