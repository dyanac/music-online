package is1.controller;


import is1.bean.AlbumBean;
import is1.service.AlbumService;
import is1.service.ArtistService;
import is1.service.SongService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
@RequestMapping(value = "admin/albums")
public class AlbumController {
	
	@Autowired
    private ArtistService authorService;

    @Autowired
    private AlbumService albumService;
    
    @Autowired
    private SongService songService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String listAlbums(Model model) {
        model.addAttribute("authenticated", true);
        model.addAttribute("albums", albumService.getAlbums(0, 10));
        model.addAttribute("authors", authorService.getAuthors(0, 10));
        model.addAttribute("albumBean",new AlbumBean());
        return "admin/albums";
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String newAlbums(@Valid @ModelAttribute AlbumBean albumBean,
                             Errors errors, ModelMap model) {
        if (!errors.hasErrors()) {
            albumService.createAlbum( albumBean.createAlbum() );
            return "redirect:/admin/albums";
        }
        model.addAttribute("errors", errors);
        return "admin/albums";
    }
}
