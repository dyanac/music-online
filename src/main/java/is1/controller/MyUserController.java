package is1.controller;

import is1.model.MyUser;
import is1.repository.MyUserRepository;
import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
class MyUserController {

	@Autowired
	private MyUserRepository UserRepository;

	@RequestMapping(value = "User/current", method = RequestMethod.GET)
	public String user(Principal principal) {
		return "redirect:/" + principal.getName() + "/playlists";
	}

	@RequestMapping(value = "{username}", method = RequestMethod.GET)
	public String user(@PathVariable String username) {
		return "redirect:/" + username + "/playlists";
	}

	@RequestMapping(value = "{username}/follow", method = RequestMethod.POST)
	public String follow(@PathVariable String username, Principal principal) {
		MyUser User = UserRepository.findByUsername(principal.getName());
		MyUser UserToFollow = UserRepository.findByUsername(username);
		if (UserToFollow != null) {
			//User.addFollowing(UserToFollow.getFeed());
			UserRepository.merge(User);
		}
		return "redirect:/" + username + "/playlists";
	}
}
