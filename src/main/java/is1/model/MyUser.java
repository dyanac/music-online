package is1.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "user")
@NamedQuery(name = MyUser.FIND_BY_USERNAME, query = "select a from MyUser a where a.username = :username")
public class MyUser implements BaseEntity<Long> {

	public static final String FIND_BY_USERNAME = "MyUser.findByUsername";
	
	@Id
	@SequenceGenerator(name = "MYUSER_ID_GENERATOR", sequenceName = "MYUSER_ID_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MYUSER_ID_GENERATOR")
	private Long id;

	@Column(unique = true, length = 20)
    private String username;

	@Column(unique = true)
	private String email;

	@JsonIgnore
	private String password;
	
	@ManyToMany
	@JoinTable(name="user_user",
    joinColumns=
        @JoinColumn(name="follow_id", referencedColumnName="id"),
    inverseJoinColumns=
        @JoinColumn(name="follower_id", referencedColumnName="id")
    )
	private List<MyUser> list_follow;
	
	@ManyToMany(mappedBy = "list_follow")
	private List<MyUser> list_follower;
	
	@OneToMany(mappedBy = "owner")
	private List<Playlist> list_playlists;

	protected MyUser() {

	}
	
	public MyUser(String username) {
		this.username = username;
	}
	
    public MyUser(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

	@Override
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
	
	public List<MyUser> getFollow() {
		return list_follow;
	}
	
	public void setFollow(List<MyUser> follow) {
		this.list_follow = follow;
	}
	
	public List<MyUser> getFollower() {
		return list_follower;
	}
	
	public void setFollower(List<MyUser> follower) {
		this.list_follower = follower;
	}
	
	public List<Playlist> getPlaylists() {
		return list_playlists;
	}
	
	public void setPlaylists(List<Playlist> playlists) {
		this.list_playlists = playlists;
	}
}
