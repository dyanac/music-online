package is1.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "artist")
public class Artist implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "ARTIST_ID_GENERATOR", sequenceName = "ARTIST_ID_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ARTIST_ID_GENERATOR")
	private Long id;

	@Column
	private String name;
	
	@Column
	private Float rate;

	@ManyToMany(mappedBy = "list_artists")
	private List<Album> list_album;
	
	@ManyToMany(mappedBy = "list_artist")
	private List<Song> list_song;

	protected Artist() {

    }

	public Artist(String name) {
		this.name = name;
	}
	
    public Artist(String name, List<Album> list_album) {
        this.name = name;
        this.list_album = list_album;
    }
	
	@Override
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<Album> getArtist() {
		return this.list_album;
	}

	public void setArtist(List<Album> artist) {
		this.list_album = artist;
	}
	
	public void setRate(Float rate) {
		this.rate = rate;
	}

	public Float getRate() {
		return rate;
	}
}
