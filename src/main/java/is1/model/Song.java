package is1.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "song")
public class Song implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "SONG_ID_GENERATOR", sequenceName = "SONG_ID_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SONG_ID_GENERATOR")
	private Long id;

	@Column
	private String name;
	
	@Column
	private Float rate;
	
	@ManyToMany(mappedBy = "list_album_songs")
	private List<Album> list_albums;

	@ManyToMany(mappedBy = "list_songs")
	private List<Playlist> list_playlists;
	
	@ManyToMany
	@JoinTable(name="song_artist",
    joinColumns=
        @JoinColumn(name="song_id", referencedColumnName="id"),
    inverseJoinColumns=
        @JoinColumn(name="artist_id", referencedColumnName="id")
    )
	private List<Artist> list_artist;

	protected Song() {

    }
	
	public Song(String name) {
		this.name = name;
	}

    public Song(String name, List<Album> list_albums, List<Artist> list_artist) {
        this.name = name;
        this.list_albums = list_albums;
        this.list_artist = list_artist;
    }
	
	@Override
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Album> getAlbum() {
		return list_albums;
	}

	public void setAlbum(List<Album> list_albums) {
		this.list_albums = list_albums;
	}

	public List<Playlist> getPlaylists() {
		return list_playlists;
	}

	public void setPlaylists(List<Playlist> list_playlists) {
		this.list_playlists = list_playlists;
	}
	
	public void setRate(Float rate) {
		this.rate = rate;
	}

	public Float getRate() {
		return rate;
	}
}
