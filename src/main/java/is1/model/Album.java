package is1.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "album")
public class Album implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "ALBUM_ID_GENERATOR", sequenceName = "ALBUM_ID_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ALBUM_ID_GENERATOR")
	private Long id;

	@Column
	private String name;
	
	@Column
	private Float rate;

	@ManyToMany
	@JoinTable(name="album_song",
    joinColumns=
        @JoinColumn(name="album_id", referencedColumnName="id"),
    inverseJoinColumns=
        @JoinColumn(name="song_id", referencedColumnName="id")
    )
	private List<Song> list_album_songs;
	
	@ManyToMany
	@JoinTable(name="album_artist",
    joinColumns=
        @JoinColumn(name="album_id", referencedColumnName="id"),
    inverseJoinColumns=
        @JoinColumn(name="artist_id", referencedColumnName="id")
    )
	private List<Artist> list_artists;
	

	protected Album() {

    }
	
	public Album(String name) {
		this.name = name;
				
	}

    public Album(String name, List<Song> list_album_songs, List<Artist> list_artists) {
        this.name = name;
        this.list_album_songs = list_album_songs;
        this.list_artists = list_artists;
    }
	
	@Override
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Song> getSongs() {
		return list_album_songs;
	}

	public void setSongs(List<Song> songs) {
		this.list_album_songs = songs;
	}
	
	public List<Artist> getArtist() {
		return this.list_artists;
	}

	public void setArtist(List<Artist> artist) {
		this.list_artists = artist;
	}
	
	public void setRate(Float rate) {
		this.rate = rate;
	}

	public Float getRate() {
		return rate;
	}
}