package is1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "admin")
public class Admin implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "ADMIN_ID_GENERATOR", sequenceName = "ADMIN_ID_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ADMIN_ID_GENERATOR")
	private Long id;

	@Column
	private String name;
	
	@Column
	private String email;
	
	@Column
	private String password;
	
	@Override
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}